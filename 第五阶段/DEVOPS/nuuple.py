'''
元组是序列类型的，它有下标，每一项没有名字
命名的组员，可以为元组中的每一项起名，类似于字典的key
'''
import collections
# p1=(10,20)
Point=collections.namedtuple('Point',['x','y','z'])
p2=Point(10,20,30)
p2[0]#像元组一样用下标来找元素
p2[1]
p2[1:]#也可以切片
p2.x#也可以像OOP一样 实例.属性
p2.y
p2.z