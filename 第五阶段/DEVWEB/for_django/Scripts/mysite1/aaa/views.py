from django.shortcuts import render,HttpResponse,get_object_or_404,redirect
from .models import Question,Choice
# def index(request):
#     return HttpResponse('<h1>polls ok </h1>')
def index(request):
    questions=Question.objects.all() #获取所有的问题
    #render用于向用户返回一个网页（模版），字典是传给网页的参数
    return render(request,'aaa/index.html',{'question':questions})
def detail(request,question_id):
    # question=Question.objects.get(id=question_id)
    question=get_object_or_404(Question,id=question_id)
    return render(request,'aaa/detail.html',{'question':question})
def result(request,question_id):
    question=get_object_or_404(Question,id=question_id)
    return render(request, 'aaa/result.html', {'question': question})
def vote(request,question_id):
    choice_id=request.POST.get('c.id')
    c=get_object_or_404(Choice,id=choice_id)
    c.votes += 1
    c.save()
    return  redirect('result',question_id=question_id)
