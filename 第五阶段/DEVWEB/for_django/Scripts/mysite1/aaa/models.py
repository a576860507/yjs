from django.db import models
from django.db import models
class Question(models.Model):
    question_text=models.CharField(max_length=200)
    publish_date=models.DateTimeField('date published')
    date_hierarchy = 'publish_date'
    search_fields = ('question_text',)
    ordering = ('publish_date', 'question_text')
    def __str__(self):
        return self.question_text
class Choice(models.Model):
    choice_text=models.CharField(max_length=200)
    votes=models.IntegerField(default=0)
    question=models.ForeignKey(Question,on_delete=models.CASCADE)
    list_display = ('choice_text', 'votes', 'question')
    raw_id_fields = ('question',)
    def __str__(self):
        return self.choice_text
# Create your models here.
