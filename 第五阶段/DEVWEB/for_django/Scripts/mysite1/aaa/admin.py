from django.contrib import admin
from .models import Question,Choice

class QuestionAdmin(admin.ModelAdmin):
    list_display = ('question_text','publish_date')
    list_filter = ('publish_date',)
class ChoiceAdmin(admin.ModelAdmin):
    list_display = ('choice_text', 'votes','question')
admin.site.register(Question,QuestionAdmin)
admin.site.register(Choice,ChoiceAdmin)
# Register your models here.
